#ENV /bin/bash

source ~/.bashrc

# Pre-test commands

# Set variables
year=$(date +%Y)

# print out every command along with its output
set -x

# Now the fun begins!
cerae -h

# List notebooks
cerae -l
ls /notebooks

# Copy fixture notebook to /notebooks
cp -R /app/tests/fixtures/notes /notebooks/notes
cp /app/tests/fixtures/notebooks.yml /notebooks

# Check that it worked
ls /notebooks
ls /notebooks/notes

# Set up notebook in Cerae
cerae -r /notebooks
cerae -n Notes -p '/notebooks/notes' -c nb -r /notebooks

# Now it should be in the list
cerae -l
ls /notebooks/bin
cat /notebooks/bin/nb

# And the notebook is accessible
nb -h

# TODO
# nb file -h
# nb cmd -h

# Notebook task actions
nb task -h
nb task -a "Test add task functionality"
cat /notebooks/notes/documents/$year.yml

nb task 11 -s "try baffles"
nb task 11-1 -d # delete subtask
nb task 11 -s "add spring-loaded squirrel hurlers"
nb task 11-1 -t "Set timer on subtask"
nb task 11 -c
# # #nb task 8 -e #uncomment to test manual operation
nb task 10 -d "soft-delete task"
nb task 24 -D # hard-delete task
nb task 14 -p # postpone task

# # Notebook note actions
# nb note -h
# nb note -q -a "Oh, wow a note!"
# nb
ls /notebooks/notes/documents/$year

# #nb note 11 -e #uncomment to test manual edit
#cat $(nb note 11 -p)
# nb note -q -a "A note to delete"
# nb note 12 -d
nb
ls /notebooks/notes/documents/$year

# cat /notebooks/notes/documents/$year.yml
#vim /notebooks/notes/documents/2019.yml
#
# # View actions
nb view -l 9
nb view -i
nb view -q entry
nb view -t entry
# uncomment to verify duplicate tasks merged in new year archive
#vim /notebooks/notes/documents/2019.yml
# id 13
# should have subtasks:
# "not carried over"
# "this one will be a dupe"
# "another new subtask"

#ls /notebooks/notes/documents/2019

nb
# After successful migration:
# * no agenda
# * 2019.yml & 2020.yml
