# Cerae

A command-line interface for project-based notes and task management.

```
cerae -n <notebook name> -p <path to notebook> -c <command> -r <root path>
```

Will create a new reference to a notebook at a given path in the notebooks.yml file at the provided root path (optional-- by default this will be the home directory). This command also creates a binstub for the notebook at root path or home directory's bin folder. If you add ${rootPath}/bin to your path, the command will be available.

After adding a notebook, you will be able to call the new command directly.
For example, if you add a notebook: `cerae -n Work -p ~/Notes -c wrk`,
you can then call commands on that notebook like: `wrk -t <task>`.

## Notebook Commands

Usage: <notebook name> [options]

run commands against the given notebook

Options:
  -V, --version                  output the version number
  -a, --add [content]            add a new task with content
  -c, --complete [number]        mark the numbered task complete
  -d, --delete [number]          delete the numbered entry
  -e, --entry [id]               select entry by id
  -f, --file [path]              add a file and open it
  -F, --file-path [path]         get the file's path
  -i, --incomplete-tasks         get all incomplete tasks
  -n, --note [content]           override default entry type (task)
  -o, --output [number]          output everything
  -p, --postpone [id]            postpone a task
  -P, --path                     output the notebook path
  -q, --query [search]           query entries by search pattern
  -r, --rollover [id]            mark task for rollover
  -s, --start-stop [id]          start or stop a task
  -t, --tags [tags]              add comma separated tags
  -v, --content-value [content]  content value
  -x, --archive                  archive
  -h, --help                     output usage information


## Testing

`docker-compose run test`

## TODO

* Fix issue where agenda files are not properly migrated to notes
? Finish migrating code from agenda -> year archive
* Combine tasks that got rolled over within the same year. (confirm with current notebooks)
* create new entry-cli-presenter ->
  converts entry to {} with properties:
      id, stat, contentStr, dateStr, durationMins...

### remaining migration to years
* remove file-option-handler

### subtask improvements
* Do not hard delete any entry or subtask that is referred to by any timer
* Move subtask to its own file
* Add nextSubtaskId() to entry & add id to entry
* Allow timers to reference subtaskId

* Add byDay grouping of entries in yearArchive

#Small stuff
* Finally, update this README
