FROM node

RUN apt-get update && apt-get install -y vim
ENV EDITOR=vim

# set timezones
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /app
COPY package.json package.json
RUN npm install
VOLUME /app/node_modules

#Add 'cerae' command to path
RUN echo 'PATH=$PATH:/app/bin' >> ~/.bashrc

RUN mkdir -p /notebooks/bin
VOLUME /notebooks
RUN echo 'PATH=$PATH:/notebooks/bin' >> ~/.bashrc
