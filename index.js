const cerae = require('./lib/cerae.js');
const ceraeNotebooks = require('./lib/cerae/notebooks.js');
const config = require('./lib/cerae/config.js');
const ceraeDigest = require('./lib/cerae-digest.js');
const notebook = require('./lib/notebook.js');
const notebookDirectoryHelper = require('./lib/helpers/notebook-directory.js');
const notebookInitializer = require('./lib/notebook-initializer.js');
const program = require('commander');
const printer = require('./lib/helpers/cli-table-printer.js');
const yaml = require('./lib/helpers/yaml.js');

program
  .version('0.0.1')
  .option('-c, --command [command]',   'set command name of notebook')
  .option('-d, --digest',              'display incomplete tasks for all notebooks')
  .option('-l, --list',                'display list of notebooks')
  .option('-n, --name [name]',         'set a notebook name')
  .option('-p, --path [path]',         'set path of notebook')
  .option('-r, --root [path]',         'add new root path to config')
  .option('-s, --setup',               'set bin paths for notebook paths')
  .option('-T, --task-in-progress',    'get current working process in notebooks')
  .parse(process.argv); // end with parse to parse through the input

class Cerae {
  constructor(params) {
    this.params = params;
    this.path = __dirname + "/notebooks.yml";
  }

  get isCreatingNotebookCommand() {
    return this.params.name && this.params.command && this.params.path;
  }

  get config() {
    return config.new({
      basePath: __dirname,
      yamlr: yaml,
      notebookDirectoryHelper: notebookDirectoryHelper
    });
  }

  get notebooks() {
    return ceraeNotebooks.new(this.config.notebooksHome, this.newNotebook, yaml).list;
  }

  containsNotebookFromParams() {
    return this.notebooks.find(n => n.name == params.name) ||
           this.notebooks.find(n => n.commmand == params.command);
  }

  newNotebook(params) {
    return notebook.new(params);
  }
}

function exec() {
  let c = new Cerae(program);
  if (c.isCreatingNotebookCommand) {
    let newNotebook = c.newNotebook(c.params);
    notebookInitializer.new(newNotebook, __dirname).exec();
  } else if (program.digest) {
    ceraeDigest.print(c.notebooks);
  } else if (program.list) {
    printer.print(["Cerae Notebooks:"], c.notebooks, {
      columns: ['command', 'name', 'path']
    });
  } else if (program.root) {
    c.config.addNotebooksHome(program.root);
  } else if (program.setup) {
    c.config.setupHomesPaths();
  } else if (program.taskInProgress) {
    ceraeDigest.taskInProgress(c.notebooks);
  } else {
    program.outputHelp();
  }
}

module.exports = {
  exec: exec
}
