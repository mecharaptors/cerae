const notebookDirectoryHelper = require('./helpers/notebook-directory.js');
const executable = require('./helpers/executable.js');
const file = require('./helpers/file.js');
const yaml = require('./helpers/yaml.js');

class NotebookInitializer {
  constructor(notebook, ceraeNotebooksPath) {
    this.notebook = notebook;
    this.ceraeNotebooksPath = ceraeNotebooksPath;
  }

  get rootPath() {
    if (this.notebook.root) {
      return this.notebook.root;
    } else {
      return this.ceraeNotebooksPath;
    }
  }

  get commandPath() {
    return this.rootPath + "/bin/" + this.notebook.command;
  }

  get notebooksPath() {
    return this.rootPath + "/notebooks.yml";
  }

  exec() {
    notebookDirectoryHelper.initializeRoot(this.rootPath);
    notebookDirectoryHelper.initialize(this.notebook.path);
    if (!file.exists(this.commandPath)) {
      executable.writeExecutable(this.commandPath, [
        "#!/usr/bin/env node\n",
        `const path = "${this.notebook.path}";\n`,
        `const name = "${this.notebook.name}";\n`,
        `require('${this.ceraeNotebooksPath}/lib/notebook-command-parser.js').exec(path, name);`
      ]);
      console.log("created new binstub:", this.commandPath);
    }
    yaml.writeOrAppend(this.notebooksPath, {
      name: this.notebook.name,
      path: this.notebook.path,
      command: this.notebook.command
    });
  }
}

function newNotebookInitializer(notebook, ceraeNotebooksPath) {
  return new NotebookInitializer(notebook, ceraeNotebooksPath);
}

module.exports = {
  new: newNotebookInitializer
}
