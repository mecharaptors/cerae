class EntrySubtask {
  constructor(params, baseId, index) {
    this.id = '' + baseId + '-' + (index + 1);
    this.task = params.task;
    this.completedAt = params.completedAt ? moment(params.completedAt) : null;
  }

  get persistableHash() {
    return {
      task: this.task,
      completedAt: (this.completedAt ? this.completedAt.toISOString() : null)
    }
  }

  get displayColor() {
    return this.completedAt ? 243 : 217;
  }

  get displayHash() {
    let stat = this.completedAt ? '\u2714  ' : ' _';
    return {
      id: this.id,
      stat: stat,
      entry: this.task
    };
  }

}

function newSubtask(params) {
  return new EntrySubtask(params);
}

module.exports = {
  new: newSubtask
}
