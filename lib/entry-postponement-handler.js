class EntryPostponementHandler {
  constructor(params, notebook, agenda) {
    this.params = params;
    this.notebook = notebook;
    this.agenda = agenda;
  }

  get validPostpone() {
    return !!this.agendaEntryToPostpone;
  }

  get agendaEntryToPostpone() {
    return this.agenda.entries.find(e => e.id == this.params.postpone);
  }

  exec() {
    let messages = [];
    if (this.validPostpone) {
      messages.push(`Postponed entry (${this.agendaEntryToPostpone.id}): ${this.agendaEntryToPostpone.content}`);
      this.agendaEntryToPostpone.postponed = true;
      this.agendaEntryToPostpone.agenda.saveEntries();
    }
    return messages;
  }
}

function newEntryPostponementHandler(params, notebook, agenda) {
  return new EntryPostponementHandler(params, notebook, agenda);
}

module.exports = {
  new: newEntryPostponementHandler
}
