const moment = require('moment');
let agenda = require('./agenda.js');
const commandQueue = require('./command-queue.js');
const notebookFile = require('./notebook-file.js');
let directory = require('./helpers/notebook-directory.js');
const tagParser = require('./helpers/tag-parser.js');
const path = require('path');
const yaml = require('./helpers/yaml.js');
const yearArchive = require('./year-archive.js');

class Notebook {
  constructor(params) {
    this.name = params.name;
    this.command = params.command;
    this.path = params.path;
    this.root = params.root;
  }

  get currentAgenda() {
    return this.newAgenda({ date: moment().day(0) });
  }

  get commandQueues() {
    let commands = this.path + "/commands.yml";
    return yaml.load(commands, commandQueue.new);
  }

  get currentYearArchive() {
    let year = moment().year();
    let archive = this.archiveForYear(year);
    if (archive.entries.length === 0) { archive = this.mergeEntriesFromLegacyAgendae(year) } // FORCES correct object handoff
    return archive;
  }

  archiveForYear(year) {
    let yearPath = `${this.path}/documents/${year}.yml`;
    return yaml.loadSingular(yearPath, yearArchive.new) || this.newYearArchiveForYear(year);
  }

  get archivesByYear() {
    let byYearHash = {};
    let yearArchives = yaml.loadAllYaml(`${this.path}/documents`, yearArchive.new);
    if (yearArchives) {
      yearArchives.forEach(a => byYearHash[a.name] = a);
    }
    return byYearHash;
  }

  get notebookFiles() {
    let documents = this.path + "/documents.yml";
    return yaml.load(documents, this.newNotebookFile);
  }

  get activeNotebookFiles() {
    return this.notebookFiles.filter(f => !f.archived);
  }

  get sortedNotebookFiles() {
    return this.activeNotebookFiles.sort((a, b) => {
      return ('' + a.name).localeCompare(b.name);
    });
  }

  get incompleteTasks() {
    return this.allEntries().filter(e => {
      return e.isActiveIncomplete;
    })
  }

  nextCommandId() {
    let commandqueues = this.commandQueues.map(q => q.id);
    let maxId = commandqueues.length === 0 ? 0 : Math.max.apply(Math, commandqueues);
    return maxId + 1;
  }

  saveFiles(files) {
    let documents = this.path + "/documents.yml";
    yaml.write(documents, files.map(f => f.persistableHash));
  }

  saveCommandQueues(queues) {
    let path = this.path + "/commands.yml";
    yaml.write(path, queues.map(q => q.persistableHash))
  }

  get agendae() {
    let numWeeks = parseInt(this.params.output);
    if (numWeeks > 0) {
      let length = this.notebook.agendae.length - numWeeks;
      return this.notebook.agendae.slice(length);
    } else {
      return this.notebook.agendae;
    }
  }

  // TODO : fetch agenda by year, and then
  // filter its entries by day instead of getting allEntries()
  entriesByDay() {
    return this.allEntries().sort((a, b) => {
      return a.updatedAt - b.updatedAt;
    }).reduce((hash, e) => {
      let key = e.updatedAt.format("YYYYMMDD");
      if (hash[key]) {
        hash[key].push(e);
      } else {
        hash[key] = [e];
      }
      e.subtasks.filter(st => st.completedAt).forEach(st => {
        let key = st.completedAt.format("YYYYMMDD");
        if (hash[key]) {
          if (!hash[key].map(stored => stored.id).includes(e.id)) {
            hash[key].push(e);
          }
        } else {
          hash[key] = [e];
        }
      })
      return hash;
    }, {});
  }

  allEntriesWithSearchString(searchString) {
    let str = searchString.toLowerCase();
    return this.allEntries().filter(e => {
      return e.matchesText(str);
    });
  }

  allEntriesWithTag(tagStr) {
    let tags = tagParser.exec(tagStr).map(t => t.toLowerCase());
    return this.allEntries().filter(e => {
      return e.type != "file" &&
        tags.find(t => e.tags.map(t => t.toLowerCase()).includes(t));
    });
  }

  allFilesWithTag(tagStr) {
    let tags = tagParser.exec(tagStr).map(t => t.toLowerCase());
    return this.allEntries().filter(e => {
      return e.type === "file" &&
        tags.find(t => e.tags.map(t => t.toLowerCase()).includes(t));
    });
  }

  allEntries() {
    let archives = Object.values(this.archivesByYear);
    return archives.reduce((acc, a) => {
      return acc.concat(a.entries);
    }, []).sort((a, b) => {
      return a.fullId - b.fullId;
    });
  }

  mergeEntriesFromLegacyAgendae(year) {
    let loadedYearArchives = this.archivesByYear;
    let entryIdMap = {}
    this.agendae.forEach(a => {
      a.loadEntries();
      a.entries.forEach(e => {
        let ey = e.createdAt.year();
        if (!loadedYearArchives[ey]) {
          loadedYearArchives[ey] = this.newYearArchiveForYear(ey);
        }
        let duplicateEntry = this._duplicateEntry(e, loadedYearArchives[ey].entries);
        if (duplicateEntry) {
          duplicateEntry.completedAt = e.completedAt;
          this._mergeSubtasks(e, duplicateEntry);
          return;
        }
        e.resumesId = entryIdMap[e.resumesId] || null;
        entryIdMap[e.id] = loadedYearArchives[ey].pushNewEntry(e.persistableHash).id;
      });
    });
    Object.values(loadedYearArchives).forEach(a => a.save());
    return loadedYearArchives[year];
  }

  _duplicateEntry(entry, entries) {
    // ignore duplication of content in timers
    return entries.find(e => {
      return e.content === entry.content &&
             e.type === "task";
    });
  }

  _mergeSubtasks(entry, duplicate) {
    entry.subtasks.forEach(st => {
      let match = duplicate.subtasks.find(t => t.task == st.task);
      if (match && !st.completedAt && match.completedAt) {
        st.completedAt = match.completedAt
      } else if (!match) {
        duplicate.subtasks.push(st);
      }
    });
  }

  // TODO: refresh tags in notes' yaml frontmatter here
  allTags() {
    let tags = [];
    this.allEntries().forEach(e => {
      e.tags.forEach(t => {
        let stored = tags.find(s => s.tag.toLowerCase() == t.toLowerCase());
        if (stored) {
          stored.entries.push(e.id);
        } else {
          tags.push({ tag: t, entries: [e.id]});
        }
      });
    });
    return tags.sort(function(a, b) {
      return ('' + a.tag.toLowerCase()).localeCompare(b.tag.toLowerCase());
    });
  }

  newCommandQueue(params) {
    let queue = commandQueue.new({
      notebook: this,
      id: this.nextCommandId(),
      name: params.name
    });
    let path = this.path + "/commands.yml";
    yaml.writeOrAppend(path, queue.persistableHash);
    return queue;
  }

  newYearArchiveForYear(year) {
    return yearArchive.new({
      name: year,
      basePath: this.path,
      relativePath: `documents/${year}.yml`
    });
  }

  newAgenda(params) {
    params.notebook = this;
    return agenda.new(params);
  }

  newNotebookFile(params) {
    return notebookFile.new(params);
  }

  agendaByDate(date) {
    let agendaDate = moment(date.format("YYYYMMDD")).day(0); // Make sure we don't change given moment
    return this.agendae.find(a => a.date.isSame(agendaDate, 'day'));
  }

  get agendae() {
    let agendaPath = this.path + "/agenda";
    let dateStrs = directory.agendaDateStringsAtDir(agendaPath);
    let notebook = this;
    return dateStrs.map((d) => {
      return notebook.newAgenda({ dateStr: d });
    });
  }
}

function newNotebook(params) {
  return new Notebook(params);
}

module.exports = {
  new: newNotebook
}
