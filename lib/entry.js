const moment = require('moment');
const clc = require('cli-color');
const fs = require('fs');
const Decorator = require('./helpers/entry-decorator.js');
const presenter = require('./entry-cli-presenter.js');
const cliLink = require('./helpers/cli-file-link.js');
const tagParser = require('./helpers/tag-parser.js');
const ansiEscapes = require('ansi-escapes');

class EntrySubtask {
  constructor(params, baseId, index) {
    this.id = '' + baseId + '-' + (index + 1);
    this.task = params.task;
    this.completedAt = params.completedAt ? moment(params.completedAt) : null;
  }

  get persistableHash() {
    return {
      task: this.task,
      completedAt: (this.completedAt ? this.completedAt.toISOString() : null)
    }
  }

  get displayColor() {
    return this.completedAt ? 243 : 217;
  }

  get displayHash() {
    let stat = this.completedAt ? '\u2714  ' : ' _';
    return {
      id: this.id,
      stat: stat,
      entry: this.task
    };
  }

}

class Entry {
  constructor(params) {
    this.id = params.id;
    this.content = params.content;
    this.type = params.type || "task";
    this.createdAt = moment(params.createdAt);
    this.completedAt = params.completedAt ? moment(params.completedAt) : null;
    this.deletedAt = params.deletedAt ? moment(params.deletedAt) : null;
    this.deleteMessage = params.deleteMessage;
    this.notePath = params.notePath || '';
    this.postponed = params.postponed || false;
    this.resumedByAgenda = params.resumedByAgenda ? moment(params.resumedByAgenda) : null;
    this.resumesId = params.resumesId;
    this.rollover = params.rollover || false;
    this.subtasks = subtaskMapper(this.id, params.subtasks);
    this.minutesOnTask = params.minutesOnTask || 0;
    this.tags = params.tags || [];
    this.yearArchive = params.yearArchive;
  }

  get contentForPersistence() {
    if (this.type === "file") {
      return this.filename;
    } else if (!this.content) {
      return "";
    } else {
      return this.content;
    }
  }

  get decorator() {
    return Decorator.new({ entry: this, maxArchiveId: this.yearArchive.nextId()});
  }

  get basePath() {
    if (this.type === "file") {
      return this.agenda.notebook.path + "/by-date/";
    }
  }

  get path() {
    if (this.type === "file") {
      return this.basePath + this.filename;
    }
  }

  get filename() {
    if (this.type === "file") {
      return this.createdAt.format("YYYY-MM-DD") + ".md";
    }
  }

  get fullId() {
    let year = this.createdAt.year();
    return `${year}-${this.id}`;
  }

  get isInCurrentYear() {
    return this.createdAt.year() === moment().year();
  }

  get sortDate() {
    return this.completedAt || this.deletedAt || this.createdAt;
  }

  get isActiveIncomplete() {
    return this.type === "task" &&
           !this.completedAt &&
           !this.deletedAt &&
           !this.isRolledOver &&
           !this.resumedByAgenda;
  }

  get isRolledOver() {
    return this.rollover === true && !this.agenda.isCurrentAgenda();
  }

  get isInactiveTimer() {
    return !!(this.type === "timer" && this.completedAt);
  }

  get inProgress() {
    return this.type === "task" && this.inProgressTimer;
  }

  get inProgressTimer() {
    return this.yearArchive.activeTimerFor(this.id);
  }

  get incompleteSubtasks() {
    return this.subtasks.filter(st => !st.completedAt);
  }

  get cliFileWithLink() {
    if (this.type === "file") {
      // TODO : re-implement file features
      //return cliLink.cliLink(this.basePath, this.filename);
    } else if (this.type === "note" && this.notePath) {
      return cliLink.cliLinkBare(this.notePath, this.content);
    }
  }

  get timers() {
    return this.yearArchive.timersFor(this.id);
  }

  get durationMinutes() {
    if (this.type === "timer") {
      return this.timerMinutes.asMinutes();
    } else if (this.type === "task") {
      //return this.agenda.totalMinutesForTask(this.id);
    }
  }

  get timerMinutes() {
    if (this.type === "timer") {
      let duration;
      if (this.completedAt) {
        duration = moment.duration(this.completedAt.diff(this.createdAt));
      } else {
        duration = moment.duration(moment().diff(this.createdAt));
      }
      return duration;
    }
  }

  get duration() {
    if (this.type === "timer") {
      let duration;
      if (this.completedAt) {
        duration = moment.duration(this.completedAt.diff(this.createdAt));
      } else {
        duration = moment.duration(moment().diff(this.createdAt));
      }
      return duration;
    }
    return 0;
  }

  get updatedAt() {
    return this.completedAt || this.deletedAt || this.createdAt;
  }

  get toCLIPresenter() {
    return presenter.new(this);
  }

  get persistableHash() {
    return {
      id: this.id,
      completedAt: (this.completedAt ? this.completedAt.toISOString() : null),
      content: this.contentForPersistence,
      createdAt: this.createdAt.toISOString(),
      deletedAt: (this.deletedAt ? this.deletedAt.toISOString() : null),
      deleteMessage: this.deleteMessage || null,
      minutesOnTask: this.minutesOnTask,
      notePath: this.notePath || '',
      postponed: this.postponed,
      resumedByAgenda: this.resumedByAgenda ? this.resumedByAgenda.toISOString() : null,
      resumesId: this.resumesId || null,
      rollover: this.rollover,
      subtasks: this.subtasks.map(s => s.persistableHash),
      tags: this.tags,
      type: this.type
    }
  }

  addSubtask(subtaskContent) {
    this.subtasks.push(new EntrySubtask({
      task: subtaskContent
    }));
  }

  matchesText(stringSearch) {
    let str = stringSearch.toLowerCase();
    return this.content.toLowerCase().includes(str) ||
      this.tags.includes(str) ||
      (this.deleteMessage && this.deleteMessage.toLowerCase().includes(str)) ||
      this.subtaskMatchesText(str) || this.noteMatchesText(str);
  }

  subtaskMatchesText(stringSearch) {
    return this.subtasks.find(st => st.task.toLowerCase().includes(stringSearch.toLowerCase()));
  }

  noteMatchesText(stringSearch) {
    if (this.type === "note") {
      try {
        let fullText = fs.readFileSync(this.notePath, 'utf8');
        return fullText.toLowerCase().includes(stringSearch.toLowerCase());
      } catch(err) {
        require('./actions/note-actions.js').fixLegacyNote(this);
      }
    }
  }

  updateFromHash(params) {
    this.content = params.content;
    if (params.createdAt) {
      this.createdAt = moment(params.createdAt);
    }
    this.completedAt = params.completedAt ? moment(params.completedAt) : null;
    this.deletedAt = params.deletedAt ? moment(params.deletedAt) : null;
    this.deleteMessage = params.deleteMessage;
    this.postponed = params.postponed || false;
    this.resumesId = params.resumesId;
    this.rollover = params.rollover || false;
    this.subtasks = subtaskMapper(this.id, (params.subtasks || []));
    this.tags = params.tags || [];
  }

  update(params) {
    if (this.type === "task" && params.complete) {
      this.completedAt = moment();
      this.rollover = false;
      this.subtasks.filter(st => !st.completedAt).forEach(st => {
        st.completedAt = moment();
      })
    }

    if (this.type === "task" && params.add) {
      this.subtasks.push(new EntrySubtask({
        task: params.add
      }))
    }

    if (params.tags) {
      let newTags = tagParser.exec(params.tags);
      this.tags = [...new Set(this.tags.concat(newTags))];
    }

    if (params.minutes) {
      this.minutesOnTask += parseInt(params.minutes);
    }

    if (params.contentValue) {
      this.content = params.contentValue;
    }
  }

  updateSubtask(index, params) {
    let subtask = this.subtasks[index - 1];
    if (subtask && params.complete) {
      subtask.completedAt = moment();
    }
  }

  postpone() {
    if (this.yearArchive) {
      this.yearArchive.postponeEntry(this);
    } else {
      throw new Error("Missing yearArchive")
    }

  }

  removeSubtask(index) {
    return this.subtasks.splice((index - 1), 1);
  }

  save() {
    if (this.yearArchive) {
      this.yearArchive.save();
    } else {
      throw new Error("Missing yearArchive");
    }
  }

  delete() {
    if (this.yearArchive) {
      this.yearArchive.removeEntry(this);
    } else {
      throw new Error("Missing yearArchive")
    }
  }

  updateFile(data) {
    if (data.tags && data.tags.length != this.tags.length &&
        data.tags.find(t => !this.tags.includes(t))) {

      this.tags = [...new Set(this.tags.concat(data.tags))];
      return true;
    }
    return false;
  }
}

function subtaskMapper(baseId, params = []) {
  return params.map((p, index) => {
    return new EntrySubtask(p, baseId, index);
  });
}

function newEntry(obj) {
  return new Entry(obj);
}

module.exports = {
  new: newEntry
}
