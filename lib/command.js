class Command {
  constructor(params, queueId) {
    this.commandQueueId = queueId;
    this.name = params.name;
    this.confirmBefore = !!params.confirmBefore;
  }

  get persistableHash() {
    return {
      name: this.name,
      confirmBefore: this.confirmBefore
    }
  }
}

function newCommand(params, queueId) {
  return new Command(params, queueId);
}

module.exports = {
  new: newCommand
}
