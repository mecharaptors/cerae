const homedir = require('os').homedir();

class Config {
  constructor(params) {
    this.basePath = params.basePath;
    this.yamlr = params.yamlr;
    this.notebookDirectoryHelper = params.notebookDirectoryHelper;
  }

  get path() {
    return homedir + "/.cerae-config";
  }

  get notebooksHome() {
    let notebooks = [homedir].concat(this.yamlr.load(this.path));
    return [...new Set(notebooks)];
  }

  addNotebooksHome(homePath) {
    if (!this.notebooksHome.includes(homePath)) {
      this.yamlr.writeOrAppend(this.path, homePath);
    }
  }

  setupHomesPaths() {
    this.notebooksHome.map(home => {
      this.notebookDirectoryHelper.initializeRoot(home);
    });
  }
}

function newConfig(params) {
  return new Config(params);
}

module.exports = {
  new: newConfig
}
