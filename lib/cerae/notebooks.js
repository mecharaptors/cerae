class Notebooks {
  constructor(configHomes, nbPrototypeFunc, yamlr) {
    this.configHomes = configHomes;
    this.nbPrototypeFunc = nbPrototypeFunc;
    this.yamlr = yamlr;
  }

  get list() {
    let entries = [];
    this.configHomes.forEach(basePath => {
      let path = basePath + "/notebooks.yml";
      entries.push(...this.yamlr.load(path, this.nbPrototypeFunc));
    });
    return entries.sort((a,b) => {
      return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
    });
  }
}

function newNotebooks(...params) {
  return new Notebooks(...params);
}

module.exports = {
  new: newNotebooks
}
