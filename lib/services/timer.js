const moment = require('moment');
const clc = require('cli-color');

class Timer {
  constructor(notebook, params) {
    this.notebook = notebook;
    this.yearArchive = this.notebook.currentYearArchive;
    this.id = params.id ? params.id.split("-") : null;
    this.params = params;
  }

  get entryId() {
    return this.id ? this.id[0] : null;
  }

  get subtaskId() {
    return this.id ? this.id[1] : null;
  }

  get referenceEntry() {
    return this.yearArchive.entries.find(e => {
      return e.id == this.entryId;
    });
  }

  get referenceContent() {
    if (this.referenceEntry && this.subtaskId) {
      let subtask = this.referenceEntry.subtasks[(this.subtaskId - 1)];
      return subtask ? "WIP: " + subtask.task : "WIP";
    } else if (this.referenceEntry) {
      return "WIP";
    }
  }

  get referenceTags() {
    return this.referenceEntry ? this.referenceEntry.tags.slice(0) : [];
  }

  get startedTimer() {
    return this.yearArchive.entries.find(e => {
      return e.type === "timer" && !e.completedAt;
    });
  }

  get isNotATimerOption() {
    return !this.params.toggleTimer;
  }

  get content() {
    let timerContent = (typeof this.params.toggleTimer === "string") ? this.params.toggleTimer : null;
    return timerContent || this.referenceContent || "<unspecified>";
  }

  rolloverOldEntry(referenceEntry) {
    if (!referenceEntry) { return false; }
    if (referenceEntry.isInCurrentYear) {
      return referenceEntry;
    } else {
      referenceEntry.resumedByAgenda = moment().year();
      referenceEntry.save();
      return this.yearArchive.pushNewEntry({
        createdAt: moment(),
        resumesId: referenceEntry.id,
        content: referenceEntry.content,
        type: referenceEntry.type,
        tags: referenceEntry.tags
      });
    }
  }

  exec() {
    if (this.isNotATimerOption) { return; }
    if (this.startedTimer) {
      this.startedTimer.completedAt = moment();
      this.startedTimer.save();
    } else {
      let referenceEntry = this.referenceEntry;
      let rolloverEntry = this.rolloverOldEntry(referenceEntry);
      let resumesRealTarget = rolloverEntry ? rolloverEntry : this.referenceEntry;
      this.createNewTimer(resumesRealTarget);
      this.yearArchive.save();
    }
  }

  createNewTimer(resumesRealTarget) {
    return this.yearArchive.pushNewEntry({
      type: "timer",
      resumesId: (resumesRealTarget ? resumesRealTarget.id : null),
      content: this.content,
      tags: this.referenceTags
    });
  }
}

function newTimer(agenda, params) {
  return new Timer(agenda, params);
}

module.exports = {
  new: newTimer
}
