const childProcess = require('child_process');
const fs = require('fs');
const moment = require('moment');
const pathOS = require('path');
const process = require('process');
const yaml = require("../helpers/yaml.js");
var Entry = require("../entry.js");

function addTask(notebook, options) {
  options.type = "task";
  let currentYearArchive = notebook.currentYearArchive;
  let entry = _getNewEntry(currentYearArchive, options);
  if (typeof options.add === "string") {
    currentYearArchive.save();
    console.log(`Added (${entry.id}) - ${entry.content}`);
  } else {
    let tempPath = `${process.cwd()}/add-${entry.id}.txt`;
    _writeFile(tempPath, entry);
    _openInEditor(tempPath, entry, options);
  }
}

function editTask(task, options) {
  if (task) {
    let tempPath = `${process.cwd()}/edit-${task.id}.txt`;
    _writeFile(tempPath, task, options);
    _openInEditor(tempPath, task, options);
  }
}

function addNote(notebook, options) {
  let now = moment();
  let notePath = `${notebook.path}/documents/${now.format("YYYY")}/${now.format("MM-DD-HH-mm")}.txt`;
  options.type = "note";
  options.notePath = notePath;
  let currentYearArchive = notebook.currentYearArchive;
  let entry = _getNewEntry(currentYearArchive, options);
  _makeDirectoriesIfMissing(notePath);

  _writeFile(notePath, entry);
  if (options.skipEditor) {
    currentYearArchive.save();
    return entry;
  } else {
    _openInEditor(notePath, entry, options);
  }
}

function editNote(note, options) {
  if (note && note.notePath) {
    _openInEditor(note.notePath, note, options);
  }
}

function editCommandQueue(queue, allQueues, notebook) {
  let path = '.tmp-queue-editor.yml';
  yaml.write(path, [queue.persistableHash]);
  let editor = process.env.EDITOR;
  let editorSpawn = childProcess.spawn(editor, [path], {
    stdio: 'inherit',
    detached: true
  });

  editorSpawn.on('close', function(data){
    queue.update(yaml.load(path)[0]);
    notebook.saveCommandQueues(allQueues);
    fs.unlinkSync(path);
  });
}

function _getNewEntry(yearArchive, options) {
  let entry = yearArchive.pushNewEntry({
    content: options.add,
    notePath: options.notePath,
    type: options.type
  });
  return entry;
}

function _makeDirectoriesIfMissing(filePath) {
  var dirname = pathOS.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  _makeDirectoriesIfMissing(dirname);
  fs.mkdirSync(dirname);
}

function _openInEditor(path, entry, options) {
  let editor = process.env.EDITOR;
  let editorSpawn = childProcess.spawn(editor, [path], {
    stdio: 'inherit',
    detached: true
  });

  editorSpawn.on('close', function(data){
    entry.updateFromHash(yaml.loadFrontmatter(path));
    entry.save();
    if (entry.type === "task") {
      fs.unlinkSync(path);
    }
  });
}

function _writeFile(path, entry, options={}) {
  let writer = fs.createWriteStream(path);
  writer.write("---\n");
  writer.write(`content: ${entry.content}\n`);
  if (entry.type === "task" && options.edit) {
    writer.write(`completedAt: ${(entry.completedAt ? entry.completedAt.toISOString() : "null")}\n`);
    writer.write(`rollover: ${entry.rollover}\n`);
    writer.write("subtasks: \n");
    entry.subtasks.forEach(s => {
      writer.write(` - task: ${s.task}\n`);
      writer.write(`   completedAt: ${s.completedAt ? s.completedAt.toISOString() : ""}\n`);
    });
    // todo when reimplementing tags
    // entry.tags.forEach(t => {
    //   writer.write(` - ${t}`)
    // })
  } else if (entry.type === "task") {
    writer.write(`rollover: ${entry.rollover}\n`);
    writer.write("subtasks: []\n");
    writer.write("tags: []\n");
  } else if (entry.type === "timer") {
    writer.write(`completedAt: ${(entry.completedAt ? entry.completedAt.toISOString() : "null")}\n`);
    writer.write(`createdAt: ${entry.createdAt.toISOString()}\n`);
  }
  writer.write("---\n\n");
  writer.end();
}

module.exports = {
  addTask: addTask,
  addNote: addNote,
  editNote: editNote,
  editTask: editTask,
  editCommandQueue: editCommandQueue
}
