const moment = require('moment');
const ansiEscapes = require('ansi-escapes');

class NotebookFile {
  constructor(params) {
    this.name = params.name;
    this.archived = params.archived || false;
    this.copiedAt = params.copiedAt ? moment(params.copiedAt) : moment();
    this.tags = params.tags || [];
  }

  // nb: this is not the full path
  get path() {
    return "documents/" + this.name;
  }

  get persistableHash() {
    return {
      name: this.name,
      archived: this.archived,
      tags: this.tags,
      copiedAt: this.copiedAt.toISOString()
    }
  }
}

function newNotebookFile(params) {
  return new NotebookFile(params);
}

module.exports = {
  new: newNotebookFile
}
