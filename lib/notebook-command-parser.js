var program = require('commander');
const process = require('process');
const commandActions = require('./actions/command-actions.js');
const fileActions = require('./actions/file-actions.js');
const noteActions = require('./actions/note-actions.js');
const taskActions = require('./actions/task-actions.js');
const viewActions = require('./actions/view-actions.js');
const notebook = require('./notebook.js');

function exec(path, name) {
  let nb = notebook.new({path: path, name: name});

  program
    .command("task [id]")
    .description("add/edit tasks")
    .option('-a, --add [content]', 'add a new task with content')
    .option('-c, --complete', 'mark the task complete')
    .option('-d, --soft-delete [content]', 'mark the entry as deleted with optional message') // must delete line 24
    .option('-D, --delete', 'delete the entry from the agenda')
    .option('-e, --edit', 'edit an entry')
    .option('-p, --postpone', 'postpone a task')
    .option('-s, --subtask <content>', 'add a new subtask with content')
    .option('-t, --toggle-timer [content]', 'toggle timer on task with optional description') // must delete line 38
    .action(function(id, options) {
      taskActions.handle(nb, {
        id: id,
        add: options.add,
        complete: options.complete,
        edit: options.edit,
        softDelete: options.softDelete,
        hardDelete: options.delete,
        postpone: options.postpone,
        subtask: options.subtask,
        toggleTimer: options.toggleTimer
      });
    });

  program
    .command("note [id]")
    .option('-a, --add [content]', 'add a new note with content')
    .option('-d, --delete', 'delete the note from the agenda')
    .option('-e, --edit', 'edit the note in an editor')
    .option('-p, --path', 'print the note path')
    .option('-q, --quick', 'skips the editor')
    .option('-u, --upgrade', 'fix broken note') // TEMPORARY OPTION
    .action(function(id, options) {
      noteActions.handle(nb, {
        id: id,
        add: options.add,
        delete: options.delete,
        edit: options.edit,
        path: options.path,
        skipEditor: options.quick,
        upgrade: options.upgrade
      })
    });

  program
    .command("file [id]")
    .option('-a, --add [content]', 'add a new note with content')
    .option('-d, --delete', 'delete the note from the agenda')
    .action(function(id, options) {
      fileActions.handle(nb, {
        id: id,
        add: options.add,
        delete: options.delete,
        edit: options.edit
      })
    });

  program
    .command("cmd [id]")
    .option('-a, --add <name>', 'add a command to a queue')
    .option('-e, --edit', 'edit a command queue')
    .option('-d, --delete', 'delete a command queue')
    .option('-n, --new <name>', 'create a new command queue')
    .option('-r, --run', 'run a command queue')
    .action(function(id, options) {
      commandActions.handle(nb, id, options);
    });

  program
    .command("view")
    .option('-a, --active-timer', 'view the active timer')
    .option('-i, --incompleteTasks', 'view all the incomplete tasks')
    .option('-l, --log [number]', 'view the past n days')
    .option('-q, --query [string]', 'view entries matching a query string')
    .option('-r, --remaining-tasks', 'view remaining tasks')
    .option('-t, --tally-view [string]', 'view tally of tasks based on search string')
    .action(function(options) {
      viewActions.handle(nb, options);
    });

    program.parse(process.argv);
    if (process.argv.length < 3) {
      viewActions.handle(nb, {});
    }
}

module.exports = {
  exec: exec
}
