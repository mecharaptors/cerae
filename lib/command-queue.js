const Command = require('./command.js');

class CommandQueue {
  constructor(params) {
    this.notebook = params.notebook;
    this.id = params.id;
    this.name = params.name;
    this.commands = commandsMapper(this.id, params.commands);
  }

  get persistableHash() {
    return {
      id: this.id,
      name: this.name,
      commands: this.commands.map(c => c.persistableHash)
    }
  }

  addCommand(params) {
    this.commands.push(Command.new(params, this.id))
  }

  update(params) {
    console.log("update called with params", params)
    if (params.name) {
      this.name = params.name;
    }
    if (params.commands) {
      this.commands = commandsMapper(this.id, params.commands)
    }
  }
}

function commandsMapper(queueId, params=[]) {
  return params.map(p => {
    return Command.new(p, queueId);
  });
}

function newCommandQueue(params) {
  return new CommandQueue(params);
}

module.exports = {
  new: newCommandQueue
}
