const entryEditor = require('../services/entry-editor.js');
const execSync = require('child_process').execSync;
const file = require('../helpers/file.js');
const fs = require('fs');
const moment = require('moment');
const readline = require('readline');

function handle(notebook, id, options) {
  if (id) {
    let commandQueues = notebook.commandQueues;
    let q = commandQueues.find(c => c.id == id);
    if (!q) {
      console.log(`CommandQueue with id ${id} does not exist`);
      return
    }
    if (options.add) {
      q.addCommand({ name: options.add });
      notebook.saveCommandQueues(commandQueues);
    } else if (options.edit) {
      entryEditor.editCommandQueue(q, commandQueues, notebook);
    } else if (options.delete) {
      notebook.saveCommandQueues(commandQueues.filter(c => c.id != q.id));
    } else if (options.run) {
      runCommandQueue(notebook, q);
    }
  } else {
    if (options.new) {
      let queue = notebook.newCommandQueue({ name: options.new });
      console.log(`made new command queue with name ${queue.name} (${queue.id})`);
    } else {
      console.log("Available commands:")
      notebook.commandQueues.forEach(q => {
        console.log(`${q.id}.  ${q.name}`);
      })
    }
  }
}

async function runCommandQueue(notebook, queue) {
  let note = entryEditor.addNote(notebook, { add: `Run ${queue.name}`, skipEditor: true });
  let writeStream = fs.createWriteStream(note.notePath, {flags:'a'});
  writeStream.write("                                                        \n");
  queue.commands.forEach(c => {
    if (c.confirmBefore) {
      // TODO this does not work yet -> need to use promises
      //confirmBeforeRunning(c.name, writeStream);
    } else {
      executeCommand(c.name, writeStream);
    }
  });
  writeStream.end();
}

function confirmBeforeRunning(command, writeStream) {
  let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question(`About to run ${command}. Ready? y/n/quit`, answer => {
    if (answer === "y") {
      executeCommand(command, writeStream);
    } else if (answer === "quit"){
      console.log("you chose to quit.")
      process.exit(1);
    } else {
      confirmBeforeRunning(command, writeStream);
    }
  });
}

function executeCommand(name, writeStream) {
  let timestamp = moment().format("dddd, MMMM Do YYYY, h:mm a");
  console.log(`(${timestamp}) <${name}>\n`);
  writeStream.write(`(${timestamp}) <${name}>\n`);

  let stdout = execSync(name, { shell: '/bin/bash' });
  console.log(stdout.toString());
  writeStream.write(stdout);
  writeStream.write("\n\n")
}

module.exports = {
  handle: handle
}
