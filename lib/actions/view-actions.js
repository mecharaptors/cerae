const moment = require('moment');
const clc = require('cli-color');
const colorizeFields = require('../helpers/color-field.js').colorizeFields;

function handle(notebook, options) {
  if (options.activeTimer) {
    console.log("activeTimer on ye now");
  } else if (options.log) {
    let days = (typeof options.log === "boolean") ? 1 : parseInt(options.log, 10);
    printLog(notebook, days);
  } else if (options.incompleteTasks) {
    console.log("Incomplete Tasks:")
    notebook.incompleteTasks.forEach(t => {
      console.log("%s   %s", t.decorator.spacePadFullId, t.content);
    });
  } else if (options.query) {
    printSearchResults(notebook, options.query);
  } else if (options.remainingTasks) {
    printRemainingTasks(notebook);
  } else if (options.tallyView) {
    printSearchResultsWithTally(notebook, options.tallyView);
  } else {
    printLog(notebook, 1);
    printRemainingTasks(notebook);
  }
}

function printLog(notebook, numDays) {
  let entriesByDay = notebook.entriesByDay();
  let relevantKeys = Object.keys(entriesByDay).reverse().slice(0, numDays);
  relevantKeys.forEach(k => {
    printLogDay(entriesByDay, k);
  });
}

function printLogDay(entries, key) {
  let day = moment(key);
  let duration = _getCollectionDuration(entries[key]);
  console.log(getDayHeader(day, duration, duration));
  let entryDisplayHashes = [];
  entries[key].forEach(e => {
    if (e.updatedAt.format("YYYYMMDD") === key) {
      entryDisplayHashes.push(e.decorator.decoratedHash);
    }
    getSubtasks(e, key).forEach(st => entryDisplayHashes.push(st));
  });
  entryDisplayHashes.sort((a,b) => {
    return a.date.valueOf() - b.date.valueOf();
  }).forEach(h => {
    printEntryHash(h);
  });
  console.log();
}

function printEntryHash(entryHash) {
  let colorized = colorizeFields(entryHash.colorScheme, entryHash);
  console.log("%s   %s  %s %s", colorized.id,
    colorized.stat,
    colorized.timeStr,
    colorized.content)
}

function getSubtasks(entry, key) {
  let subtasks = entry.subtasks.filter(st => {
    return st.completedAt && st.completedAt.format("YYYYMMDD") === key;
  });
  return subtasks.map(st => {
    return {
      id: st.id,
      stat: clc.xterm(st.displayColor)(st.displayHash.stat),
      date: st.completedAt,
      dateStr: clc.xterm(st.displayColor)(st.completedAt.format("hh:mmA")),
      timeStr: clc.xterm(st.displayColor)(st.completedAt.format("hh:mmA")),
      content: clc.xterm(st.displayColor)(`${st.task} [${entry.id}: ${entry.content}]`),
    };
  })
}

function printRemainingTasks(notebook) {
  let entries = notebook.currentYearArchive.incompleteTasks;
  console.log("Remaining tasks:");
  entries.forEach(e => {
    let entryView = e.toCLIPresenter;
    console.log("%d  %s %s ", e.id,
      entryView.detailedHash.stat,
      entryView.detailedHash.entry);

    e.subtasks.filter(e => !e.completedAt).forEach(s => {
      console.log("%s %s %s", s.id, clc.xterm(217)(" _   "), clc.xterm(217)(s.task));
    });
  });
  console.log();
}

function printSearchResults(notebook, searchTerm) {
  _getSearchResultHashes(notebook, searchTerm).forEach(h => {

    printEntryHash(h);
  });
}

function printSearchResultsWithTally(notebook, searchTerm) {
  let results = _getSearchResultHashes(notebook, searchTerm) || [];
  let resultsByDay = results.reduce((dayHash, entry) => {
    let key = entry.date.format("YYYYDDDD");
    if (dayHash[key]) {
      dayHash[key].push(entry);
    } else {
      dayHash[key] = [entry];
    }
    return dayHash;
  }, {});
  let relevantKeys = Object.keys(resultsByDay).reverse();
  relevantKeys.forEach(k => {
    _printSearchDay(notebook, resultsByDay, k);
  });
}

function _printSearchDay(notebook, resultsByDay, key) {
  let day = moment(key);
  let archive = notebook.archiveForYear(day.year());
  let totalDuration = archive.totalTimerDurationOnDay(day);
  let entriesDuration = _getCollectionDuration(resultsByDay[key]);
  console.log(getDayHeader(day, entriesDuration, totalDuration));
  resultsByDay[key].forEach(h =>{ printEntryHash(h) });
  console.log();
}

function getDayHeader(day, entriesDuration, totalDuration) {
  let date = day.format("dddd, MMMM Do YYYY");
  if (totalDuration > 0 && totalDuration > entriesDuration) {
    let points = _getPoints(entriesDuration.asMinutes(), totalDuration.asMinutes());
    return `${date} (${points}pts ~ ${entriesDuration.asMinutes()}/${totalDuration.asMinutes()} mins)`
  } else if (totalDuration > 0) {
    return `${date} (${totalDuration.humanize()} logged)`
  } else {
    return `${date} (No time tracked)`
  }
}

function _getCollectionDuration(collection) {
  return collection.reduce((sum, r) => {
    if (r.momentDuration) {
      sum.add(r.momentDuration);
    } else if (r.timerMinutes) {
      sum.add(r.timerMinutes);
    }
    return sum;
  }, moment.duration());
}

function _getPoints(entriesMins, totalMins) {
  let percentMins = entriesMins / totalMins;
  if (percentMins >= 0.75) {
    return 2;
  } else if (percentMins < 0.75 && percentMins > 0.5) {
    return 1.5;
  } else if (percentMins <= 0.5 && percentMins > 0.25) {
    return 1;
  } else {
    return 0.5;
  }
}

function _getSearchResultHashes(notebook, searchTerm) {
  let entries = notebook.allEntriesWithSearchString(searchTerm);
  let entryDisplayHashes = []
  entries.forEach(e => {
    let entryView = e.toCLIPresenter;
    if (e.subtaskMatchesText(searchTerm)) {
      let matchingSubtasks = e.subtasks.filter(st => st.task.toLowerCase().includes(searchTerm.toLowerCase()));
      matchingSubtasks.forEach(st => {
        let stV = st.displayHash;
        entryDisplayHashes.push({
          id: st.id,
          stat: stV.stat,
          date: (st.completedAt ? st.completedAt : e.updatedAt),
          dateStr: (stV.completedAt ? st.completedAt.format("ddd MMM D, h:mm a") : entryView.detailedHash.fullDate),
          timeStr: (stV.completedAt ? st.completedAt.format("h:mm a") : entryView.detailedHash.fullDate),
          content: `${entryView.detailedHash.entry} - ${st.task}`
        })
      });
    } else {
      entryDisplayHashes.push(e.decorator.decoratedHash);
      e.timers.forEach(t => {
        tV = t.toCLIPresenter;
        entryDisplayHashes.push(e.decorator.decoratedHash);
      });
      e.subtasks.forEach(st => {
        let stV = st.displayHash;
        entryDisplayHashes.push({
          id: st.id,
          stat: stV.stat,
          date: (st.completedAt ? st.completedAt : e.updatedAt),
          dateStr: (stV.completedAt ? st.completedAt.format("ddd MMM D, h:mm a") : entryView.detailedHash.fullDate),
          timeStr: (stV.completedAt ? st.completedAt.format("h:mm a") : entryView.detailedHash.fullDate),
          content: `${e.content} | ${st.task}`
        })
      });
    }
  });
  return entryDisplayHashes.sort((a, b) =>{
    return a.date.valueOf() - b.date.valueOf();
  });
}

module.exports = {
  handle: handle
}
