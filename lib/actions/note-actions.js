const entryEditor = require('../services/entry-editor.js');
const fs = require('fs');
const pathOS = require('path'); // TEMPORARY
const yaml = require('../helpers/yaml.js');

function handle(notebook, options) {
  if (options.add) {
    entryEditor.addNote(notebook, options);
  } else if (options.delete) {
    let note = _getNote(notebook, options.id);
    if (note) {
      deleteNote(note);
    }
  } else if (options.edit) {
    let note = _getNote(notebook, options.id);
    if (note) {
      entryEditor.editNote(note, options);
    }
  } else if (options.upgrade) { // TODO: remove when no more legacy notes remain!
    let note = _getNote(notebook, options.id);
    if (note) {
      fixLegacyNote(note);
    }
  } else if (options.path) {
    let note = _getNote(notebook, options.id);
    if (note && note.notePath) {
      console.log(note.notePath);
    }
  }
}

function deleteNote(note) {
  note.delete();
  if (note.notePath) {
    fs.unlinkSync(note.notePath);
  }
}

function _getNote(notebook, id) {
  if (id) {
    return notebook.currentYearArchive.entries.find(e => (e.id == id && e.type === "note"));
  }
}

// TEMPORARY ACTION
function fixLegacyNote(note) {
  if (!note.notePath) {
    let basePath = `${note.yearArchive.basePath}/documents`;
    note.notePath = `${basePath}/${note.createdAt.format("YYYY")}/${note.createdAt.format("MM-DD-HH-mm")}.txt`;
    console.log("this must be a legacy item: ", note.content, note.notePath);
    _makeDirectoriesIfMissing(note.notePath);
    _writeFile(note.notePath, note);
    note.save();
  }
}

// TEMPORARY copypasta from entry-editor
function _makeDirectoriesIfMissing(filePath) {
  var dirname = pathOS.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  _makeDirectoriesIfMissing(dirname);
  fs.mkdirSync(dirname);
}

//TEMPORARY copypasta from entry-editor ()
function _writeFile(path, entry, options={}) {
  let writer = fs.createWriteStream(path);
  writer.write("---\n");
  writer.write(`content: ${entry.content}\n`);
  writer.write("---\n\n");
  writer.end();
}


module.exports = {
  handle: handle,
  fixLegacyNote: fixLegacyNote
}
