const entryEditor = require('../services/entry-editor.js');
const moment = require('moment');
const timerService = require('../services/timer.js');
const yaml = require('../helpers/yaml.js');

function handle(notebook, options) {
  if (options.add) {
    entryEditor.addTask(notebook, options);
  } else if (options.toggleTimer && !options.id) {
    timer(notebook, options);
  } else {
    // all the remaining cannot work without an entry
    let [entryId, subtaskId] = options.id.split("-");
    subtaskId = subtaskId ? (parseInt(subtaskId, 10) - 1) : null;
    let entry = _getExistingEntry(notebook, entryId);
    if (!entry) {
      console.log("Could not find entry with id: ", entryId);
      return;
    }
    handleEntryEditActions({
      entry: entry,
      notebook: notebook,
      options: options,
      subtaskId: subtaskId,
    });
  }
}

function handleEntryEditActions(params) {
  let options = params.options;
  if (options.complete && (typeof params.subtaskId == "number")) {
    completeSubtask(params.entry, params.subtaskId);
  } else if (options.complete) {
    completeTask(params.entry);
  } else if ((typeof params.subtaskId == "number") && (options.hardDelete || options.softDelete)) {
    deleteSubtask(params.entry, params.subtaskId);
  } else if (options.softDelete) {
    softDeleteTask(params.entry, options.softDelete);
  } else if (options.hardDelete) {
    params.entry.delete();
  } else if (options.edit) {
    entryEditor.editTask(params.entry, options);
  } else if (options.postpone) {
    params.entry.postpone();
  } else if (options.subtask) {
    subtask(params.entry, options.subtask);
  } else if (options.toggleTimer) {
    timer(params.notebook, options);
  }
}

function completeSubtask(entry, subtaskId) {
  let subtask = entry.subtasks[subtaskId];
  if (subtask) {
    let now = moment();
    subtask.completedAt = now;
    _markTimersAsComplete(entry, now);
    entry.save();
  }
}

function completeTask(entry) {
  let now = moment();
  entry.completedAt = now;
  entry.rollover = false;
  entry.subtasks.filter(st => !st.completedAt).forEach(st => {
    st.completedAt = now;
  });
  _markTimersAsComplete(entry, now);
  entry.yearArchive.save();
}

function deleteSubtask(entry, subtaskId) {
  let subtask = entry.subtasks[subtaskId];
  if (subtask) {
    entry.subtasks.splice(subtaskId, 1);
    entry.save();
  }
}

function rolloverTask(notebook, task) {

}

function rolloverToCurrentAgenda(notebook, options) {

}

function softDeleteTask(entry, deleteMessage) {
  entry.deletedAt = moment();
  entry.deleteMessage = deleteMessage;
  entry.save();
}

function subtask(entry, subtaskContent) {
  entry.addSubtask(subtaskContent);
  entry.save();
}

function timer(notebook, options) {
  timerService.new(notebook, options).exec();
}

function _markTimersAsComplete(entry, now) {
  let timer = entry.yearArchive.activeTimers.find(t => t.resumesId == entry.id);
  if (timer) {
    timer.completedAt = now;
  }
}

function _getExistingEntry(notebook, id) {
  return notebook.currentYearArchive.entries.find(e => e.id == id);
}

module.exports = {
  handle: handle
}
