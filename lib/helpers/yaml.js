const fs = require('fs');
const pathOS = require('path');
const yaml = require('js-yaml');
const yamlFront = require('yaml-front-matter');

function load(path, mapFunc, errFunc) {
  let raw;
  try {
    raw = yaml.safeLoad(fs.readFileSync(path, "utf8"));
    if (raw) {
      if (mapFunc) {
        return raw.map((x) => { return mapFunc(x); });
      } else {
        return raw;
      }
    } else {
      return [];
    }
  } catch(e) {
    if (errFunc) { errFunc(e) };
    return [];
  }
}

// load all .yml files in directory at path
// and map by mapFunc
function loadAllYaml(path, callbackFunc) {
  try {
    let files = fs.readdirSync(path).filter(f => {
      return pathOS.extname(f) === ".yml";
    });
    return files.map(file => {
      let filePath = `${path}/${file}`;
      let raw = yaml.safeLoad(fs.readFileSync(filePath, "utf8"));
      if (raw) {
        return callbackFunc(raw);
      } else {
        return null;
      }
    }).filter(f => !!f);
  } catch(e) {
    console.log("loadAllYaml failed for ", path, " with ", e);
  }
}

function loadSingular(path, callback, errFunc) {
  try {
    let raw = yaml.safeLoad(fs.readFileSync(path, "utf8"));
    if (raw) {
      return callback(raw);
    } else {
      return null; // TODO return notFoundCallback
    }
  } catch(e) {
    if (errFunc) { errFunc(e) };
    return null; // TODO return notFoundCallback
  }
}

function loadFrontmatter(path) {
  if (exists(path)) {
    return yamlFront.loadFront(fs.readFileSync(path, "utf8"));
  } else {
    return {}
  }
}

function append(path, object) {
  try {
    fs.writeFileSync(path, yaml.safeDump([object]), { flag: 'a' }, function(err) {
      if (err) throw err;
    });
  } catch (e) {
    console.log('This err was not thrown. Go figure.', e);
  }
}

function write(path, objects) {
  try {
    fs.writeFileSync(path, yaml.safeDump(objects), function(err) {
      if (err) throw err;
    });
  } catch (e) {
    console.log('This err was not thrown. Go figure.', e);
  }
}

function writeSingular(path, object) {
  try {
    let undefVal = Object.keys(object).find(k => object[k] == undefined);
    if (undefVal) {
      throw `Undefined value at key: ${undefVal}`;
    }
    fs.writeFileSync(path, yaml.safeDump(object), function(err) {
      if (err) throw err;
    });
  } catch(e) {
    console.log(`Error thrown while writing to ${path}`, e);
  }
}

function exists(path) {
  try {
    return fs.existsSync(path);
  } catch(e) {
    console.log("exists err; file does not exist?");
    return false;
  }
}

function writeOrAppend(path, object) {
  if (exists(path)) {
    append(path, object);
  } else {
    write(path, [object]);
  }
}

module.exports = {
  append: append,
  exists: exists,
  load: load,
  loadAllYaml: loadAllYaml,
  loadSingular: loadSingular,
  loadFrontmatter: loadFrontmatter,
  write: write,
  writeSingular: writeSingular,
  writeOrAppend: writeOrAppend
}
