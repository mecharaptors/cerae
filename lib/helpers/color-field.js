const clc = require('cli-color');

function colorizeFields(color, object) {
  let returnObj = {};
  Object.keys(object).forEach(key => {
    returnObj[key] = clc.xterm(color)(object[key]);
  });
  return returnObj;
}

module.exports = {
  colorizeFields: colorizeFields
}
