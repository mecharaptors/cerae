const moment = require('moment');
const fs = require('fs');

function write(fileEntry) {
  writeFileIfMissing(fileEntry.path, fileEntry);
}

function writeFileIfMissing(path, fileEntry) {
  let fstat;
  try {
    fstat = fs.statSync(path);
  } catch(e) {
    let writer = fs.createWriteStream(path);
    writer.write("---\n");
    writer.write(`date: ${fileEntry.createdAt.format("YYYY-MM-DD")}\n`);
    writer.write("title:\n");
    writer.write("tags:\n");
    fileEntry.tags.forEach(t => {
      writer.write(` - ${t}\n`)
    });
    writer.write("---\n\n");
    writer.end();
  }
}

module.exports = {
  write: write
}
