const fs = require('fs');
const path = require('path');
var yaml = require('./yaml.js');
const process = require('process');

function append(filePath, content) {
  let stream = fs.createWriteStream(filePath, {flags:'a'});
  stream.write(content);
  stream.end();
}

function basename(filePath) {
  return path.basename(filePath);
}

function copyFile(originPath, destinationDirectory) {
  try {
    let originBasename = basename(originPath);
    let destinationPath = destinationDirectory + "/" + originBasename;
    //console.log("Yah im not doing that yet", originBasename, destinationPath);
    fs.copyFileSync(originPath, destinationPath);
    return true;
  } catch(e) {
    console.log("something went wrong copying file at path", originPath);
    return false;
  }
}

function editLive(entry) {
  let path = `${process.cwd()}/edit-${entry.id}.txt`;
  let writer = fs.createWriteStream(path);
  let entryOutput = entry.persistableHash;
  writer.write("---\n");
  writer.write(`completedAt: ${entryOutput.completedAt}\n`);
  writer.write(`content: ${entryOutput.content}\n`);
  writer.write(`createdAt: ${entryOutput.createdAt}\n`);
  writer.write(`deletedAt: ${entryOutput.deletedAt}\n`);
  writer.write(`deleteMessage: ${entryOutput.deleteMessage}\n`);
  writer.write(`resumedByAgenda: ${entryOutput.resumedByAgenda}\n`);
  writer.write(`resumesId: ${entryOutput.resumesId}\n`);
  writer.write(`rollover: ${entryOutput.rollover}\n`);
  writer.write("subtasks:\n");
  entry.subtasks.forEach(t => {
    writer.write(` - task: ${t.task}\n`);
    writer.write(` - completedAt: ${t.completedAt}\n`);
  });
  writer.write("tags:\n");
  entry.tags.forEach(t => {
    writer.write(` - ${t}\n`)
  });
  writer.write("---\n\n");
  writer.end();

  var editorSpawn = require('child_process').spawn(process.env.EDITOR, [path], {
    stdio: 'inherit',
    detached: true
  });

  editorSpawn.on('close', function(data){
    entry.updateFromHash(yaml.loadFrontmatter(path));
    entry.agenda.saveEntries();
    fs.unlinkSync(path);
    process.exit(1);
  });
}

function exists(filePath) {
  try {
    return fs.existsSync(filePath);
  } catch(e) {
    console.log("exists err; file does not exist?", e);
    return false;
  }
}

function hasExtension(filePath) {
  return !!path.extname(filePath);
}

function touch(filePath) {
  try {
    fs.writeFileSync(filePath, '', 'utf8');
  } catch(e) {
    console.log("error while touching file", e);
    return false;
  }
}

module.exports = {
  append: append,
  basename: basename,
  copyFile: copyFile,
  editLive: editLive,
  exists: exists,
  hasExtension: hasExtension,
  touch: touch
}
