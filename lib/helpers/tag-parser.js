
function exec(tagsStr) {
  if (tagsStr) {
    return tagsStr.split(",");
  } else {
    return [];
  }
}

module.exports = {
  exec: exec
}
