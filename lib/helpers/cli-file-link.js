const ansiEscapes = require('ansi-escapes');

function cliFileWithLink(basePath, file) {
  let fullPath = "file://" + basePath + "/" + file.path;
  return ansiEscapes.link(file.name, fullPath);
}

function cliLink(basePath, name) {
  let fullPath = "file://" + basePath + name;
  return ansiEscapes.link(name, fullPath);
}

function cliLinkBare(path, name) {
  let fullPath = "file://" + path;
  return ansiEscapes.link(name, fullPath);
}


module.exports = {
  cliFileWithLink: cliFileWithLink,
  cliLink: cliLink,
  cliLinkBare: cliLinkBare
}
