const columnify = require('columnify');

function print(headings, objects, options={}) {
  if (options.clearConsole) {
    process.stdout.write('\033[2J'); // clear console
    process.stdout.write('\033[H'); // move cursor to home! (upper left)
  }

  headings.forEach(heading => {
    console.log(heading);
  });
  console.log(columnify(objects, options));
}

module.exports = {
  print: print
}
