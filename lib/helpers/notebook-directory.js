const fs = require('fs');
const path = require('path');
const homedir = require('os').homedir();

// Still need to reload ~/.bashrc after this :(
// Not a complete solution since docker wipes out bashrc every time
function initializeRootBinstubs(rootDir) {
  mkdirIfMissing(rootDir + "/bin");
}

function initializeDirectory(dir) {
  mkdirIfMissing(dir);
  mkdirIfMissing(dir + "/agenda");
  mkdirIfMissing(dir + "/by-date");
  mkdirIfMissing(dir + "/documents");
}

function agendaDateStringsAtDir(dir) {
  return fs.readdirSync(dir, { withFileTypes: true }).filter((f) => {
    return path.extname(f.name) === ".yml";
  }).map((f) => {
    return path.basename(f.name, ".yml");
  });
}

function mkdirIfMissing(dir) {
  let fstat;
  try {
    fstat = fs.statSync(dir);
  } catch(e) {
    console.log("Creating directory at ", dir);
    fs.mkdirSync(dir, { recursive: true });
  }
}

module.exports = {
  initialize: initializeDirectory,
  initializeRoot: initializeRootBinstubs,
  agendaDateStringsAtDir: agendaDateStringsAtDir,
  mkdirIfMissing: mkdirIfMissing
}
