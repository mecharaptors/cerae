const clc = require('cli-color');
const colorizeFields = require('./color-field.js').colorizeFields;

class EntryDecorator {
  constructor(params) {
    this.entry = params.entry;
    this.maxIdDigits = params.maxArchiveId.toString().length;
  }

  get decoratedHash() {
    return {
      id: this.spacePadId,
      colorScheme: this.colorScheme,
      content: this.contentForRow,
      date: this.entry.updatedAt,
      dateStr: this.dateStr,
      duration: this.duration,
      fullId: this.spacePadFullId,
      stat: this.stat,
      timeStr: this.timeStr
    };
  }

  get contentForRow() {
    let entry = this.entry;
    if (entry.type === "task" && entry.completedAt) {
      return `${entry.content}`;
    } else if (entry.type === "task" && entry.deletedAt) {
      if (entry.deleteMessage) {
        return `DEL[${entry.content}] - ${entry.deleteMessage}`;
      } else {
        return `DEL[${entry.content}]`;
      }
    } else if (entry.type === "task") {
      return `${entry.content}`;
    } else if (entry.type === "timer" && entry.resumesId) {
      let r = entry.yearArchive.entries.find(e => e.id == entry.resumesId);
      if (r) {
        return `(${entry.duration.humanize()}) [${r.content}] : ${entry.content}`;
      }
    } else if (entry.type === "timer") {
      return `(${entry.duration.humanize()}) ${entry.content}`;
    } else if (entry.type === "note" && entry.notePath) {
      return `${entry.cliFileWithLink}`;
    } else if (entry.type === "file" && !entry.notePath) {
      return `${entry.cliFileWithLink}`;
    }
  }

  get dateStr() {
    let entry = this.entry;
    return (entry.completedAt ? entry.completedAt.format("ddd MMM D, h:mm a") : entry.fullDate);
  }

  get duration() {
    if (this.entry.type !== "timer") { return ''; }
    return `(${this.entry.timerMinutes.humanize()})`
  }

  get spacePadId() {
    let idString = this.entry.id.toString();
    let padLength = this.maxIdDigits - idString.length;
    return (' '.repeat(padLength) + idString);
  }

  get spacePadFullId() {
    let idString = this.entry.fullId.toString();
    let padLength = this.maxIdDigits + 5 - idString.length;
    return (' '.repeat(padLength) + idString);
  }

  get stat() {
    let entry = this.entry;
    if (entry.type === "task" && entry.completedAt) {
      return " [x] ";
    } else if (entry.inProgress) {
      return "  ~  ";
    } else if (entry.type === "task" && entry.resumedByAgenda) {
      return "  <  ";
    } else if (entry.type === "task" && entry.isRolledOver) {
      return "  >  ";
    } else if (entry.type === "task" && entry.postponed) {
      return "  <  ";
    } else if (entry.deletedAt) {
      return " --- ";
    } else if (entry.type === "task") {
      return " [ ] ";
    } else if (entry.type === "timer" && entry.completedAt) {
      return "  .  ";
    } else if (entry.type === "timer") {
      return "  ~  ";
    } else {
      return "  -  ";
    }
  }

  get timeStr() {
    return this.entry.updatedAt.format("h:mma");
  }

  get colorScheme() {
    let entry = this.entry;
    let baseColorInt;
    if (entry.type === "task" && !entry.completedAt && !entry.deletedAt) {
      baseColorInt = 211;
    } else if (entry.type === "task" && entry.deletedAt) {
      baseColorInt = 245;
    } else if (entry.type === "task" && entry.completedAt) {
      baseColorInt = 245;
    } else if (entry.type === "note") {
      baseColorInt = 249;
    } else {
      baseColorInt = 245;
    }
    return baseColorInt;
  }

}

function newEntryDecorator(params) {
  return new EntryDecorator(params);
}

module.exports = {
  new: newEntryDecorator
}
