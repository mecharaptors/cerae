const fs = require('fs');

function writeExecutable(path, stringsArr) {
  let writer = fs.createWriteStream(path, { mode: 0o755});
  stringsArr.forEach(function(s) {
    writer.write(s);
  });
  writer.end();
}

module.exports = {
  writeExecutable: writeExecutable
}
