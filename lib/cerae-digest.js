const columnify = require('columnify');
const clc = require('cli-color');

const colors = [
  135,
  80,
  170,
  68,
  33
];

function getColor(index) {
  if (index >= colors.length) {
    return getColor(index - colors.length);
  } else {
    return colors[index];
  }
}

function print(notebooks) {
  let outputTable = [];
  notebooks.forEach((notebook, index) => {
    let tasks = notebook.currentAgenda.incompleteTasks;
    let color = clc.xterm(getColor(index));
    tasks.forEach(task => {
      outputTable.push({
        notebook: color(notebook.command),
        id: color(task.id),
        task: color(task.content),
        tags: color(task.tags)
      })
    })
  });
  console.log(columnify(outputTable));
}

function taskInProgress(notebooks) {
  let active = notebooks.find((notebook) => {
    return notebook.currentAgenda.activeTimers.length > 0;
  });
  if (active) {
    let timer = active.currentAgenda.activeTimers[0].toCLIPresenter;
    if (timer) {
      let color = clc.xterm(67);
      console.log(color(timer.activeTimerLine));
    }
  } else {
    console.log('');
  }
}

module.exports = {
  print: print,
  taskInProgress: taskInProgress
}
