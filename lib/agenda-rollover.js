const file = require('./helpers/file.js');
const yaml = require('./helpers/yaml.js');
const moment = require('moment');

// <cmd> -r <entry-to-rollover-id> :
//    * when entry-to-rollover is from an older agenda, new entry created in current agenda that rolls over entry
//    * when entry-to-rollover is in current agenda, marks entry for rollover
// <cmd> -e <target-id> -r <entry-to-rollover-id> : target is marked as 'rollover' of 'entry-to-rollover'

class AgendaRollover {
  constructor(agenda, notebook, params) {
    this.agenda = agenda;
    this.notebook = notebook;
    this.params = params;
    this.prevAgenda = agenda.prevAgenda();
  }

  get entryToRollover() {
    let rolloverId = parseInt(this.params.rollover);
    return this.notebook.incompleteTasks.find(e => e.id == rolloverId);
  }

  get targetEntry() {
    let id = parseInt(this.params.entry);
    return this.notebook.allEntries().find(e => e.id == id);
  }

  get isRollover() {
    return this.params.rollover && this.entryToRollover;
  }

  get isCandidateForRollover() {
    return !file.exists(this.agenda.path) &&
            file.exists(this.prevAgenda.path) &&
            this.prevAgenda.incompleteTasks.filter(t => {
              return t.rollover;
            }).length > 0;
  }

  rolloverEntry() {
    let entry = this.entryToRollover;
    let target = this.targetEntry;
    if (target && entry) {
      target.resumesId = entry.id;
      entry.resumedByAgenda = target.agenda.date;
      target.agenda.saveEntries();
      entry.agenda.saveEntries();
      return `Rolled over entry ${entry.id} -> ${target.id} (${target.agendaDate.format("MM/DD/YY")}) ${entry.content}`;
    } else if (entry.agendaDate.isBefore(this.agenda.date, 'day')) {
      entry.resumedByAgenda = this.agenda.date;
      entry.agenda.saveEntries();

      let e = this.agenda.newEntry(this.agenda)({
        id: this.notebook.nextId(),
        resumesId: entry.id,
        content: entry.content,
        type: entry.type,
        tags: entry.tags,
        subtasks: entry.subtasks.filter(st => !st.completedAt)
      });

      yaml.writeOrAppend(this.agenda.path, e.persistableHash);
      return `Rolled over entry ${entry.id} from ${entry.createdAt.format("MM/DD/YY")}: ${entry.content}`;
    } else {
      entry.rollover = true;
      entry.agenda.saveEntries();
      return `Marked entry for rollover: ${entry.id}`;
    }
  }

  copyIncompleteTasks() {
    let startId = this.notebook.nextId();
    let copiedEntries = [];
    this.prevAgenda.incompleteTasks.forEach((task, index) => {
      if (task.rollover) {
        task.id = startId + index;
        task.minutes = 0; // reset minutes
        task.agendaDate = this.agenda.date; // set agendaDate to current
        task.resumesId = null;
        task.rollover = false;
        copiedEntries.push(task);
      }
    });
    yaml.write(this.agenda.path, copiedEntries.map(e => e.persistableHash));
  }
}

function newRollover(agenda, notebook, params) {
  return new AgendaRollover(agenda, notebook, params);
}

module.exports = {
  new: newRollover
}
