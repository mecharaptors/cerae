const yaml = require('./helpers/yaml.js');
const Entry = require('./entry.js');
const moment = require('moment');

class YearArchive {
  constructor(params) {
    this.name = params.name;
    this.basePath = params.basePath;
    this.relativePath = params.relativePath;
    this.entries = this.mapEntriesFromParams(params.entries);
    this.postponedTasks = this.mapPostponedTasksFromParams(params.postponedTasks);
  }

  // calculated properties

  get activeTimers() {
    return this.entries.filter(e => (e.type === "timer" && !e.completedAt ));
  }

  get incompleteTasks() {
    return this.entries.filter(e => (e.type === "task" && !e.completedAt &&!e.deletedAt));
  }

  get path() {
    return this.basePath + "/" + this.relativePath;
  }

  get persistableHash() {
    return {
      name: this.name,
      basePath: this.basePath,
      relativePath: this.relativePath,
      entries: this.persistableEntries,
      postponedTasks: this.persistablePostponedTasks
    }
  }

  get persistableEntries() {
    return this.entries.map(e => e.persistableHash);
  }

  get persistablePostponedTasks() {
    return this.postponedTasks.map(t => t.persistableHash);
  }

  get timers() {
    return this.entries.filter(e => e.type === "timer");
  }

  // Functions

  activeTimerFor(id) {
    return this.activeTimers.find(t => t.resumesId == id);
  }

  mapEntriesFromParams(entryHashes) {
    let ret = [];
    if (entryHashes) {
      entryHashes.forEach(e => {
        e.yearArchive = this;
        ret.push(Entry.new(e));
      });
    }
    return ret;
  }

  // TODO - NOT YET IMPLEMENTED
  mapPostponedTasksFromParams(postponedTaskHashes) {
    return [];
  }

  nextId() {
    let entryIds = this.entries.map(e => e.id);
    let maxId = entryIds.length === 0 ? 0 : Math.max.apply(Math, entryIds);
    return maxId + 1;
  }

  postponeEntry(entry) {
    let entryIndex = this.entries.indexOf(entry);
    this.entries.splice(entryIndex, 1);
    this.postponedTasks.push(entry);
    this.save();
  }

  pushNewEntry(entryParams) {
    let entry = Entry.new(entryParams);
    entry.id = this.nextId();
    entry.yearArchive = this;
    this.entries.push(entry);
    return entry;
  }

  removeEntry(entry) {
    let entryIndex = this.entries.indexOf(entry);
    this.entries.splice(entryIndex, 1);
    this.save();
  }

  save() {
    yaml.writeSingular(this.path, this.persistableHash);
  }

  timersOnDay(day) {
    return this.timers.filter(t => {
      return (t.completedAt && t.completedAt.isSame(day, 'day')) || t.createdAt.isSame(day, 'day');
    });
  }

  timersFor(entryId) {
    return this.timers.filter(t => t.resumesId == entryId);
  }

  totalTimerDurationOnDay(day) {
    return this.timersOnDay(day).reduce((duration, t) => {
      duration.add(t.timerMinutes);
      return duration;
    }, moment.duration());
  }
}

function newYearArchive(params) {
  return new YearArchive(params);
}

module.exports = {
  new: newYearArchive
}
