var agendaEntry = require('./entry.js');
var yaml = require('./helpers/yaml.js');
const moment = require('moment');
const tagParser = require('./helpers/tag-parser.js');

class Agenda {
  constructor(params) {
    this.date = params.dateStr ? moment(params.dateStr) : params.date;
    this.notebook = params.notebook;
    this.entries = [];
    this.loadEntries();
    if (this.isCurrentAgenda()) {
      this.parseTagsFromFiles();
    }
  }

  get path() {
    return this.notebook.path + "/agenda/" + this.date.format("YYYY-MM-DD") + ".yml";
  }

  get sortedEntries() {
    return this.incompleteTasks
      .concat(this.notes)
      .concat(this.completeTasks)
      .concat(this.soloTimers);
  }

  get sortedEntriesWithTimers() {
    return sortEntries(
      this.entries.filter(e => e.type !== "file")
    );
  }

  get files() {
    return sortEntries(this.entries.filter(e => {
      return e.type === "file";
    }));
  }

  get incompleteTasks() {
    return sortEntries(this.entries.filter((e) => {
      return e.type === "task" && !e.completedAt;
    }));
  }

  get notes() {
    return sortEntries(
      this.entries.filter(e => e.type === "note")
    );
  }

  get notesWithFile() {
    return sortEntries(
      this.entries.filter(e => {
        return e.type === "note" && e.notePath
      }));
  }

  get completeTasks() {
    return sortEntries(this.entries.filter((e) => {
      return e.type === "task" && !!e.completedAt;
    }));
  }

  get timers() {
    return this.entries.filter(e => e.type === "timer");
  }

  // this should only be 1 but plural for the naming convention
  get activeTimers() {
    return sortEntries(this.timers.filter((e) => {
      return !e.completedAt;
    }));
  }

  get inactiveTimers() {
    return sortEntries(this.timers.filter((e) => {
      return !!e.completedAt;
    }));
  }

  get soloTimers() {
    return this.timers.filter(e => !e.resumesId);
  }

  entriesForDay(day) {
    return this.sortedEntries.filter(e => {
      let timers = this.timersFor(e.id);
      return (e.completedAt && e.completedAt.isSame(day, 'day')) ||
             timers.find(t => t.completedAt && t.completedAt.isSame(day, 'day')) ||
             e.subtasks.find(st => st.completedAt && st.completedAt.isSame(day, 'day'));
    });
  }

  timersFor(id) {
    return sortEntries(this.timers.filter(t => t.resumesId == id));
  }

  timersOnDay(day) {
    return this.timers.filter(t => {
      return (t.completedAt && t.completedAt.isSame(day, 'day')) ||
        t.createdAt.isSame(day, 'day');
    });
  }

  isCurrentAgenda() {
    return this.date.isSame(moment().day(0), 'day');
  }

  newEntry(agenda) {
    return function(params) {
      params.agendaDate = agenda.date;
      params.agenda = agenda;
      return agendaEntry.new(params);
    }
  }

  loadEntries() {
    this.entries = yaml.load(this.path, this.newEntry(this));
  }

  parseTagsFromFiles() {
    let hasChanges = false;
    this.files.forEach(f => {
      let filePath = this.notebook.path + "/by-date/" + f.content;
      let data = yaml.loadFrontmatter(filePath);
      hasChanges = f.updateFile(data);
    });
    if (hasChanges) {
      this.saveEntries();
    }
  }

  saveEntries() {
    let entries = this.entries.sort((a,b) => {
      return a.id - b.id
    });
    yaml.write(this.path, entries.map(e => e.persistableHash));
  }

  totalMinutesForTask(id) {
    let timers = this.timers.filter(t => t.resumesId == id);
    return timers.reduce((sum, e) => {
      return sum + e.durationMinutes;
    }, 0);
  }

  prevAgenda() {
    return new Agenda({
      date: this.date.clone().day(-7),
      notebook: this.notebook
    })
  }
}

function newAgenda(params) {
  return new Agenda(params);
}

function sortEntries(entries) {
  return entries.sort(function(a, b) {
    if (a.sortDate.isBefore(b.sortDate)) {
      return -1;
    } else if (b.sortDate.isBefore(a.sortDate)) {
      return 1;
    } else {
      return 0;
    }
  });
}

module.exports = {
  new: newAgenda
}
