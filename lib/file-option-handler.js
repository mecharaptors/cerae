const datedFile = require('./helpers/dated-file.js');
const file = require('./helpers/file.js');
const moment = require('moment');
const notebookDirectory = require('./helpers/notebook-directory.js');
const tagParser = require('./helpers/tag-parser.js');
const yaml = require('./helpers/yaml.js');
const cliLink = require('./helpers/cli-file-link.js');

class FileOptionHandler {
  constructor(params, notebook, agenda) {
    let fileParams = params.file || params.filePath;
    this.isAgendaFile = (!fileParams.length || fileParams.length === 0);
    this.path = fileParams;
    this.notebook = notebook;
    this.agenda = agenda;
    this.params = params;
  }

  get archiveFile() {
    return this.params.archive;
  }

  get documentsPath() {
    return this.notebook.path + "/documents";
  }

  get fileToCopy() {
    return file.exists(this.path);
  }

  get notebookFiles() {
    return this.notebook.notebookFiles || [];
  }

  get notebookFilesYaml() {
    return this.documentsPath + ".yml";
  }

  get notebookFilename() {
    if (this.isAgendaFile) { return; } // not a notebook file, should return undefined
    if (file.hasExtension(this.path)) {
      return file.basename(this.path);
    } else {
      return file.basename(this.path) + ".md";
    }
  }

  agendaFileExists() {
    return this.agenda.files.find(e => e.createdAt.isSame(moment(), 'day'));
  }

  notebookFileExists(notebookFiles) {
    return notebookFiles.find(f => f.name === this.notebookFilename);
  }

  notebookFileCLILink(notebookFile) {
    return cliLink.cliFileWithLink(this.documentsPath, notebookFile.name);
  }

  exec() {
    if (this.isAgendaFile) {
      let entry = this.agendaFileExists();
      if (!entry) {
        entry = this.agenda.newEntry(this.agenda)({
          id: this.notebook.nextId(),
          createdAt: moment(),
          type: "file",
          tags: [...new Set(tagParser.exec(this.params.tags))]
        });
        this.agenda.entries.push(entry);
        datedFile.write(entry);
        this.agenda.saveEntries();
        if (this.params.filePath) {
          console.log(entry.path);
          return;
        }
        console.log("Added file: ", entry.cliFileWithLink);
      } else {
        if (this.params.filePath) {
          console.log(entry.path);
          return;
        }
        console.log("File exists: ", entry.cliFileWithLink);
      }
    } else {
      notebookDirectory.mkdirIfMissing(this.documentsPath);
      let notebookFiles = this.notebookFiles;
      let nbFileExists = this.notebookFileExists(notebookFiles);
      if (nbFileExists) {
        if (this.archiveFile) {
          nbFileExists.archived = true;
          this.notebook.saveFiles(notebookFiles);
          console.log("Archived file: ", this.notebookFileCLILink(nbFileExists));
        } else if (this.params.filePath) {
          console.log(this.documentsPath + "/" + nbFileExists.name);
          return;
        } else {
          console.log("File exists: ", this.notebookFileCLILink(nbFileExists));
        }
        return;
      }
      if (this.fileToCopy) {
        if (file.copyFile(this.path, this.documentsPath)) {
          let newFile = this.notebook.newNotebookFile({
            name: this.notebookFilename
          });
          yaml.writeOrAppend(this.notebookFilesYaml, newFile.persistableHash);
        }
      } else {
        let newFile = this.notebook.newNotebookFile({
          name: this.notebookFilename
        });
        file.touch(this.documentsPath + "/" + newFile.name);
        console.log("Added file: ", this.notebookFileCLILink(newFile));
        yaml.writeOrAppend(this.notebookFilesYaml, newFile.persistableHash);
      }
    }
  }
}

function newHandler(params, notebook, agenda) {
  return new FileOptionHandler(params, notebook, agenda);
}

module.exports = {
  new: newHandler
}
