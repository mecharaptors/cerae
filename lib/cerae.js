const fs = require('fs');
const yaml = require('./helpers/yaml.js');
const executableHelper = require('./helpers/executable.js');
const notebook = require('./notebook.js');
const notebookDirectoryHelper = require('./helpers/notebook-directory.js');

function readNotebooksYaml() {
  return yaml.load("./notebooks.yml", function(n) {
    return notebook.new(n);
  }, function(e) {
    console.log("Error loading notebooks.yml??")
  });
}

function writeNotebooksYaml(notebooks) {
  yaml.write("./notebooks.yml", notebooks);
}

function includesNewNotebook(existingNotebooks, obj) {
  return existingNotebooks.find(function(n) {
    if ((n.name === obj.name)       ||
        (n.command === obj.command) ||
        (n.path === obj.path)) {
          return true;
        }
    return false;
  })
}

function initializeNotebook(params) {
  let nb = notebook.new(params);
  let path = "./bin/" + nb.command;

  //write executable with <command> name
  executableHelper.writeExecutable(path, [
    "#!/usr/bin/env node\n",
    `const path = "${nb.path}";\n`,
    `const name = "${nb.name}";\n`,
    "require('../lib/notebook-command-parser.js').exec(path, name);"
  ]);

  notebookDirectoryHelper.initializeDirectory(nb.path);

  return nb;
}

module.exports = {
  readNotebooksYaml: readNotebooksYaml,
  writeNotebooksYaml: writeNotebooksYaml,
  includesNewNotebook: includesNewNotebook,
  initializeNotebook: initializeNotebook
}
