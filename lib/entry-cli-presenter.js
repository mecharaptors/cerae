const clc = require('cli-color');
const moment = require('moment');
const ansiEscapes = require('ansi-escapes');
const colorizeFields = require('./helpers/color-field.js').colorizeFields;
const colWidths = {
  tags: 20,
  entry: 70,
  date: 32
}
const config = {
  config: {
    tags: { maxWidth: colWidths.tags },
    stat: { align: 'center'},
    date: { maxWidth: colWidths.date, truncate: true },
    entry: { maxWidth: colWidths.entry }
  }
}

class EntryCLIPresenter {
  constructor(entry) {
    this.entry = entry;
  }

  get colorScheme() {
    return getColorScheme(this.entry);
  }

  // actually returns an array of hashes .. so maybe rename to toEntryRows()
  toHash(keys, options={}) {
    let lines = [];
    lines.push(...this.formattedEntryRows(keys, this.entryRows));
    lines.push(...this.formattedEntryRows(keys, this.deleteMessageRow));

    if (options.displayAllTimers) {
      lines.push(...this.timerRows(keys));
    } else if (options.displayInProgress) {
      lines.push(...this.formattedEntryRows(keys, this.activeTimerRow));
    } else if (options.displayLongMode && options.day) {
      lines.push(...this.timerRowsForDay(options.day, keys));
    }

    if (options.displayAllSubtasks || options.displayIncompleteSubtasks) {
      lines.push(...this.subtasks(keys, options.displayAllSubtasks));
    } else if (options.displayLongMode && options.day) {
      lines.push(...this.subtasksForDay(options.day, keys));
    }
    return lines;
  }

  subtasks(keys, includeIncompleteTasks) {
    let subtasks = includeIncompleteTasks ? this.entry.subtasks : this.entry.subtasks.filter(st => !st.completedAt);
    return subtasks.map(st => colorizeFields(st.displayColor, st.displayHash));
  }

  subtasksForDay(day, keys) {
    let subtasks = this.entry.subtasks.filter(st => st.completedAt && st.completedAt.isSame(day, 'day'));
    return subtasks.map(st => colorizeFields(st.displayColor, st.displayHash));
  }

  timerRows(keys) {
    let timers = [];
    this.entry.timers.forEach(t => {
      timers.push(...t.toCLIPresenter.toHash(keys));
    });
    return timers;
  }

  timerRowsForDay(day, keys) {
    let timers = [];
    this.entry.timers.filter(t => t.completedAt && t.completedAt.isSame(day, 'day')).forEach(t => {
      timers.push(...t.toCLIPresenter.toHash(keys));
    });
    return timers;
  }

  get activeTimerRow() {
    let timer = this.entry.inProgressTimer;
    if (timer) {
      return {
        id: [timer.id],
        stat: ['~'],
        entry: [`... started ${timer.createdAt.format("h:mma")}`],
        duration: [timer.toCLIPresenter.duration]
      }
    }
    return {};
  }

  get deleteMessageRow() {
    let entry = this.entry;
    let row = {};
    if (entry.deletedAt) {
      let message = entry.deleteMessage ? `: ${entry.deleteMessage}` : '';
      row['entry'] = this.substrate(colWidths.entry, `.. DELETED ${entry.deletedAt.format("MM/YY h:mma")}${message}`);
    }
    return row;
  }

  get entryRows() {
    return {
      id: [this.entry.id],
      stat: [this.stat],
      entry: [this.entryContent],
      tags: this.substrate(colWidths.tags, this.tags),
      date: [this.date],
      duration: [this.duration],
      agenda: [this.agendaTitle],
      added: [this.addedDate]
    }
  }

  get addedDate() {
    return this.entry.createdAt.format("ddd M/D h:mma");
  }

  get agendaTitle() {
    return this.entry.agendaDate.format("YYYY-MM-DD");
  }

  formattedEntryRows(keys, entryRows, formattedRows=[]) {
    let formatRow = {};
    let isContentAdded = false;
    keys.forEach(key => {
      if (entryRows[key] && entryRows[key][formattedRows.length]) {
        formatRow[key] = entryRows[key][formattedRows.length];
        isContentAdded = true;
      }
    });
    if (isContentAdded) {
      formattedRows.push(colorizeFields(this.colorScheme, formatRow));
      return this.formattedEntryRows(keys, entryRows, formattedRows);
    } else {
      return formattedRows;
    }
  }

  substrate(maxLength, str, arr=[]) {
    if (str.length > maxLength) {
      let str1 = str.substring(0, str.lastIndexOf(' ', maxLength));
      let str2 = str.substring(str.lastIndexOf(' ', maxLength) + 1);
      arr.push(str1);
      return this.substrate(maxLength, str2, arr);
    } else {
      arr.push(str);
      return arr;
    }
  }

  get defaultHash() {
    return {
      id: this.entry.id,
      stat: this.stat,
      entry: this.entryContent,
      tags: this.tags,
      date: this.date
    };
  }

  get detailedHash() {
    return colorizeFields(this.colorScheme, {
      id: this.entry.id,
      stat: this.stat,
      entry: this.newEntryContent,
      tags: this.tags,
      duration: this.duration,
      date: this.date,
      fullDate: this.fullDate,
      timerDuration: this.timerMinutes
    });
  }

  get stat() {
    let entry = this.entry;
    if (entry.type === "task" && entry.completedAt) {
      return " [x] ";
    } else if (entry.inProgress) {
      return "  ~  ";
    } else if (entry.type === "task" && entry.resumedByAgenda) {
      return "  <  ";
    } else if (entry.type === "task" && entry.isRolledOver) {
      return "  >  ";
    } else if (entry.type === "task" && entry.postponed) {
      return "  <  ";
    } else if (entry.deletedAt) {
      return " --- ";
    } else if (entry.type === "task") {
      return " [ ] ";
    } else if (entry.type === "timer" && entry.completedAt) {
      return "  .  ";
    } else if (entry.type === "timer") {
      return "  ~  ";
    } else {
      return "  -  ";
    }
  }

  get newEntryContent() {
    let entry = this.entry;
    if (entry.type === "task" && entry.completedAt) {
      return `${entry.content}`;
    } else if (entry.type === "task" && entry.deletedAt) {
      if (entry.deleteMessage) {
        return `DEL[${entry.content}] - ${entry.deleteMessage}`;
      } else {
        return `DEL[${entry.content}]`;
      }
    } else if (entry.type === "task") {
      return `${entry.content}`;
    } else if (entry.type === "timer" && entry.resumesId) {
      let r = entry.yearArchive.entries.find(e => e.id == entry.resumesId);
      if (r) {
        return `(${entry.duration.humanize()}) [${r.content}] : ${entry.content}`;
      }
    } else if (entry.type === "timer") {
      return `(${entry.duration.humanize()}) ${entry.content}`;
    } else if (entry.type === "note" && entry.notePath) {
      return `${entry.cliFileWithLink}`;
    } else if (entry.type === "file" && !entry.notePath) {
      return `${entry.cliFileWithLink}`;
    }
  }

  get activeTimerLine() {
    return `${this.timerContent} (${this.duration})`;
  }

  get entryContent() {
    let entry = this.entry;
    if (entry.type === "file" || entry.notePath) {
      return entry.cliFileWithLink;
    } else if (entry.type === "timer" && entry.resumesId) {
      let dateCompleted = entry.completedAt ? entry.completedAt.format(" - h:mma") : "";
      return `${entry.createdAt.format("ddd M/D h:mma")}${dateCompleted}`;
    } else if (entry.resumesId) {
      return `${entry.content} (> ${entry.resumesId})`;
    } else {
      return entry.content;
    }
  }

  get tags() {
    return this.entry.tags.join(", ");
  }

  get timerContent() {
    if (this.entry.type !== "timer") { return ''; }
    let str = `[${this.entry.id}] `;
    if (this.entry.resumesId) {
      let task = this.entry.yearArchive.incompleteTasks.find((t) => {
        return t.id == this.entry.resumesId;
      });
      if (task) {
        str = str + `~> ${task.id}..${task.content}`;
      }
    }
    if (this.entry.content) {
      str = str + `: ${this.entry.content}`;
    }
    return str;
  }

  get timerMinutes() {
    if (this.entry.type !== "timer") { return ''; }
    return `(${this.entry.timerMinutes.humanize()})`
  }

  get duration() {
    let mins = this.entry.durationMinutes;
    if (mins) {
      if (mins > 60) {
        let remainderMins = (mins % 60 > 0) ? (' ' + Math.round(mins % 60) + 'm') : '';
        return '' + Math.floor(mins / 60) + 'h' + remainderMins;
      } else {
        return '' + Math.round(mins) + 'm';
      }
    }
  }

  get date() {
    return this.entry.updatedAt.format("hh:mmA");
  }

  get fullDate() {
    return `[${this.entry.updatedAt.format("ddd MMM D, h:mm a")}]`;
  }
}

function initializePresenter(entry) {
  return new EntryCLIPresenter(entry);
}

function filenameWithLink(entry) {
  let fullPath = "file://" + entry.path;
  return ansiEscapes.link(entry.filename, fullPath);
}

function getColorScheme(entry) {
  let baseColorInt;
  if (entry.type === "task" && !entry.completedAt && !entry.deletedAt) {
    baseColorInt = 211;
  } else if (entry.type === "task" && entry.deletedAt) {
    baseColorInt = 245;
  } else if (entry.type === "task" && entry.completedAt) {
    baseColorInt = 245;
  } else if (entry.type === "note") {
    baseColorInt = 249;
  } else {
    baseColorInt = 245;
  }
  return baseColorInt;
}

module.exports = {
  new: initializePresenter,
  config: config
}
